# language: en
Feature: rechercher le classement d'un joueur avec son nom

	Scenario Outline: rechercher le classement d'un joueur avec son nom
		Given Je suis connecté au site tennis temple
		When Je clique sur la loupe
		And J'entre le nom complet du joueur <nomjoueur> et que j'appuie sur entrée
		Then Une page de résultats s'affiche avec un lien vers <nomjoueur>
		And En cliquant sur ce lien une page s'affiche avec <nomjoueur>
		And Son classement avec la structure ATP suivi d'un espace suivi du numéro

		@non_passant
		Examples:
		| nomjoueur |
		| "toto toto" |

		@passant
		Examples:
		| nomjoueur |
		| "novak djokovic" |


*** Settings ***
Resource    tennis.resource

*** Test Cases ***
classement
    Connection
    Given Je suis connecté au site tennis temple
    When Je clique sur la loupe
    And J'entre le nom complet du joueur    novak djokovic
    And Je clique sur le resultat de <nomjoueur>    novak djokovic
    Then Son classement est visible
    Deconnection




